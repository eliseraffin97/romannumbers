const { romanNumber } = require('./roman.js');

describe('romanNumber', () => {
    it('returns I when given 1', () => {
        expect(romanNumber(1)).toEqual('I');
    })
})