const romanNumber = (a) => {
    if (a === 1) return "I"
    if (a === 2)  return "II"
    if (a === 3) return "III"
}

module.exports = {romanNumber}